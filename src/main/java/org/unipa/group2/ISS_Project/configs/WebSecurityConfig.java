package org.unipa.group2.ISS_Project.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.unipa.group2.ISS_Project.repository.UserRepo;
import org.unipa.group2.ISS_Project.service.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserRepo userRepo;

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(new UserDetailsServiceImpl(userRepo)).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    //Used to encode passwords
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and().csrf().disable()
                .authorizeRequests()
                //allow the following urls to be reached without being authenticated
                .antMatchers("/reg", "/registrationForm.html", "/favicon.ico", "/api/unident/**", "/registration", "testUp", "uploads", "/Logo-Group2.png").permitAll()
                .anyRequest().authenticated()
                .and()
                //setup login so that the login form is reachable at /login
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                //use /logout as the url to perform a logout
                .permitAll().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .and()
                .headers()
                .frameOptions()
                .disable()
                //allow iframes
                .addHeaderWriter(new StaticHeadersWriter("X-FRAME-OPTIONS",
                        "SAMEORIGIN"));
        ;
    }
}