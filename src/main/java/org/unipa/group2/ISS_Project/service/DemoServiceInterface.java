package org.unipa.group2.ISS_Project.service;

import org.unipa.group2.ISS_Project.models.DemoModel;

public interface DemoServiceInterface {
    public DemoModel sayHello();
}
