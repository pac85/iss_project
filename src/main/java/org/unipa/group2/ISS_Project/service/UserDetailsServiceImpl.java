package org.unipa.group2.ISS_Project.service;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.unipa.group2.ISS_Project.classes.RegisteredUser;
import org.unipa.group2.ISS_Project.repository.UserRepo;

import java.util.ArrayList;

public class UserDetailsServiceImpl implements UserDetailsService {

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private UserRepo userRepo;

    public UserDetailsServiceImpl(UserRepo userRepository) {
        this.userRepo = userRepository;
    }

    public org.springframework.security.core.userdetails.User processLogin(String userName) {
        ArrayList roles = new ArrayList();
        roles.add(new SimpleGrantedAuthority("user"));
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(userName, false);
        if(user == null) {
            throw new UsernameNotFoundException("User is not Found");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(),
                user.getPassword(),
                roles);
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        return processLogin(userName);
    }

}
