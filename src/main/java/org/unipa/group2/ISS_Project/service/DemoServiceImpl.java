package org.unipa.group2.ISS_Project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.unipa.group2.ISS_Project.models.DemoModel;
import org.unipa.group2.ISS_Project.repository.DemoRepositoryImpl;

@Service
public class DemoServiceImpl implements DemoServiceInterface {

    @Autowired
    private DemoRepositoryImpl demoRepo;

    @Override
    public DemoModel sayHello() {
        return this.demoRepo.getDemo();
    }
}
