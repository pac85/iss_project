package org.unipa.group2.ISS_Project.utils;

import org.unipa.group2.ISS_Project.classes.RegisteredUser;
import org.unipa.group2.ISS_Project.classes.Reply;
import org.unipa.group2.ISS_Project.classes.Tag;
import org.unipa.group2.ISS_Project.controller.TeamController;

//those classes are used to pass informations to thymeleaf
public class ElClasses {
    public static class ReplyEl {
        public Integer id;
        public UserEl user;
        public Reply.State state;
        public Integer hours;

        public ReplyEl(Reply reply) {
            id = reply.getId();
            user = new UserEl(reply.getUser());
            state = reply.getState();
            hours = reply.getHours();
        }
    }

    public static class TagEl {
        public String tag;
        public String style;

        public TagEl(Tag tag) {
            this.tag = tag.getTag();
            this.style = "background: rgb("
                    + (Math.abs(tag.getId().hashCode()) % 210 + 30) +", "
                    + (Math.abs(tag.getTag().hashCode()) % 210 + 30) + ", "
                    + (Math.abs((tag.getTag() + tag.getId()).hashCode()) % 210 + 30) + ")";
        }
    }

    public static class UserEl {
        public Integer id;
        public String propic;
        public String name;
        public String email;
        public String role = "";
        public String roleStyle;

        public  UserEl(RegisteredUser user) {
            id = user.getID();
            propic = user.getPhoto();
            name = user.getName() + " " + user.getSurname();
            email = user.getEmail();

            if(propic == null || propic.isEmpty()) {
                propic = "/defaultPropic.png";
            }
        }

        public UserEl withRole(String role) {
            this.role = role;
            //creates a unique color for each role
            this.roleStyle = "background: rgb("
                    + (role.hashCode() % 210 + 30) +", "
                    + ((role+"a").hashCode() % 210 + 30) + ", "
                    + ((role+"b").hashCode() % 210 + 30) + ")";
            return this;
        }
    }

}
