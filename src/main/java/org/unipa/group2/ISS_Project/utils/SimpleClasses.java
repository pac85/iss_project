package org.unipa.group2.ISS_Project.utils;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.unipa.group2.ISS_Project.classes.Skill;

//class used to only return relevant parts of the informations in api calls
public class SimpleClasses {

    @JsonSerialize
    public static class SimpleSkill {
        public Integer id;
        public String name;

        public SimpleSkill(Skill skill) {
            this.id = skill.getID();
            this.name = skill.getName();
        }
    }

    @JsonSerialize
    public static class SimpleSeniority {
        public Integer id;
        public SimpleSkill skill;
        public Integer years;

        public SimpleSeniority(Integer id, SimpleSkill skill, Integer years) {
            this.id = id;
            this.skill = skill;
            this.years = years;
        }
    }
}
