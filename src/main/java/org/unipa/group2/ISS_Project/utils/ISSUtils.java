package org.unipa.group2.ISS_Project.utils;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

public class ISSUtils {
    //given a filename and some data it writes it to the appropiate folder
    public static String saveFile(HttpServletRequest request, String name, MultipartFile document) {
        String uploadsDir = "/uploads/";
        String realPathtoUploads =  request.getServletContext().getRealPath(uploadsDir);
        if(! new File(realPathtoUploads).exists())
        {
            new File(realPathtoUploads).mkdir();
        }

        System.out.println("Using path" + realPathtoUploads);

        String filename = "logo" + name + document.getOriginalFilename();
        try {
            document.transferTo(new File(realPathtoUploads + "/" + filename));
        }catch(IOException e){}

        return "/uploads/" + filename;
    }

}
