package org.unipa.group2.ISS_Project.classes;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.swing.text.html.Option;
import java.util.Optional;

@Entity
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"user_id", "skill_id"})}
)
public class Seniority {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private RegisteredUser user;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "skill_id", nullable = false)
    private Skill skill;

    private Integer years;

    public Seniority() {

    }

    public Seniority(RegisteredUser user, Skill skill, Integer years) {
        this.user = user;
        this.skill = skill;
        this.years = years;
    }

    public Integer getId() {
        return this.id;
    }
    public RegisteredUser getUser() { return this.user; }
    public Skill getSkill() {
        return this.skill;
    }
    public Integer getYears() {
        return this.years;
    }
}
