package org.unipa.group2.ISS_Project.classes;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Skill {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String name;

    @ManyToMany(mappedBy = "skills")
    private Set<Announcement> announcements;

    public Skill() {}

    public Skill(String name) {
        this.name = name;
    }

    public Integer getID() {
        return this.id;
    }
    public void setID(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }
}
