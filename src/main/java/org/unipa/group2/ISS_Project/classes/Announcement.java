package org.unipa.group2.ISS_Project.classes;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Announcement {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "team_id", nullable = false)
    private Team team;

    @OneToMany(mappedBy = "announcement", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Reply> replies;

    private String title;

    @Column(columnDefinition="TEXT")
    private String description;

    private Boolean active;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "Annonucement_Skill",
            joinColumns = { @JoinColumn(name = "announcement_id") },
            inverseJoinColumns = { @JoinColumn(name = "skill_id") }
    )
    private Set<Skill> skills;

    public Announcement() {}

    public Announcement(Team team, String title, String description, Set<Skill> skills) {
        this.team = team;
        this.title = title;
        this.description = description;
        this.skills = skills;
        this.active = true;
    }

    public Integer getId() {
        return id;
    }

    public Team getTeam() {
        return team;
    }

    public Set<Reply> getReplies() {
        return replies;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getActive() {
        return active;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public void setReplies(Set<Reply> replies) {
        this.replies = replies;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public void addRepy(Reply reply) {
        replies.add(reply);
    }
}
