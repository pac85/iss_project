package org.unipa.group2.ISS_Project.classes;

import javax.persistence.*;

@Entity
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"user_id", "team_id"})}
)
public class Participation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private RegisteredUser user;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "team_id", nullable = false)
    private Team team;

    private Integer hours;

    public Participation() {}

    public Participation(RegisteredUser user, Integer hours) {
        this.user = user;
        this.hours = hours;
    }

    public RegisteredUser getUser() {
        return user;
    }

    public Integer getHours() {
        return hours;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
