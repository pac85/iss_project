package org.unipa.group2.ISS_Project.classes;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"email"})}
)
public class RegisteredUser implements User, Cloneable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String name, surname, address, postcode, cv, photo, email, password;
    private LocalDate birthDate;

    private Boolean isDisabled;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Participation> participations;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Reply> replies;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Seniority> skills;

    public static class RegisteredUserBuilder {
        private Integer id;
        private String name, surname, address, postcode, cv, photo, email, password;
        private LocalDate birthDate;
        private Set<Seniority> skills;

        public RegisteredUserBuilder(String name, String surname, String email) {
            this.name = name;
            this.surname = surname;
            this.email = email;
        }

        public RegisteredUserBuilder birthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public RegisteredUserBuilder password(String password) {
            this.password = password;
            return this;
        }

        public RegisteredUserBuilder address(String address) {
            this.address = address;
            return this;
        }

        public RegisteredUserBuilder postcode(String postcode) {
            this.postcode = postcode;
            return this;
        }

        public RegisteredUserBuilder cv(String cv)  {
            this.cv = cv;
            return this;
        }

        public RegisteredUserBuilder photo(String photo) {
            this.photo = photo;
            return this;
        }

        public RegisteredUserBuilder skills(Set<Seniority> skills) {
            this.skills = skills;
            return this;
        }

        public RegisteredUser build() {
            return new RegisteredUser(this);
        }
    }

    public RegisteredUser(){

    }

    private RegisteredUser(RegisteredUserBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.surname = builder.surname;
        this.password = builder.password;
        this.birthDate = builder.birthDate;
        this.address = builder.address;
        this.postcode = builder.postcode;
        this.cv = builder.cv;
        this.photo = builder.photo;
        this.skills = builder.skills;
        this.email = builder.email;
    }

    @Override
    public Integer getID() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getSurname() {
        return this.surname;
    }

    @Override
    public String getPassword() {return this.password; }

    @Override
    public LocalDate getBirthDate() {
        return this.birthDate;
    }

    @Override
    public Boolean getDisabled() {
        return isDisabled;
    }

    @Override
    public String getAddress() {
        return this.address;
    }

    @Override
    public String getPostcode() {
        return this.postcode;
    }

    @Override
    public String getCV() {
        return this.cv;
    }

    @Override
    public String getPhoto() {
        return this.photo;
    }

    @Override
    public Iterator<Seniority> getSkills() {
        return this.skills.iterator();
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public void setID(Integer id) {
        this.id = id;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public void setDisabled(Boolean disabled) {
        isDisabled = disabled;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Override
    public void setCV(String cv) {
        this.cv = cv;
    }

    @Override
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public void setSkills(Set<Seniority> skills) {
        this.skills = skills;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void addSeniority(Seniority seniority) {
        this.skills.add(seniority);
    }

    @Override
    public Optional<Seniority> removeSkillSeniority(Skill rs) {
        return this.skills
                .stream()
                .filter(s -> s.getSkill().getID() == rs.getID())
                .findFirst()
                .map(s -> {
                    this.skills.remove(s);
                    return s;
                });

    }

    @Override
    public void removeSeniority(Seniority seniority) {
        this.skills.remove(seniority);
    }

    public Set<Participation> getParticipations() {
        return participations;
    }

    public void setParticipations(Set<Participation> participations) {
        this.participations = participations;
    }

    public Set<Reply> getReplies() {
        return replies;
    }

    public void setReplies(Set<Reply> replies) {
        this.replies = replies;
    }
}
