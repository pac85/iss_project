package org.unipa.group2.ISS_Project.classes;

import javax.persistence.*;
import java.util.Set;
import java.util.Iterator;

@Entity
public class Team implements Cloneable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pm_id", referencedColumnName = "id")
    private Participation pm;

    @OneToMany(mappedBy = "team", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Participation> members;

    private String name;

    @Column(columnDefinition="TEXT")
    private String description;

    @OneToMany(mappedBy = "team", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Announcement> announcements;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "Team_Tag",
            joinColumns = { @JoinColumn(name = "team_id") },
            inverseJoinColumns = { @JoinColumn(name = "tag_id") }
    )
    private Set<Tag> tags;

    private String logo;

    private Boolean active;

    public Team() {}

    public Team(Participation pm, Set<Participation> members, String name, String description, String logo, Set<Announcement> announcements, Tag tag, Set<Tag> tags) {
        this.pm = pm;
        this.members = members;
        this.name = name;
        this.description = description;
        this.logo = logo;
        this.announcements = announcements;
        this.tags = tags;
        this.tags.add(tag);
        this.active = true;
    }

    public Integer getId() {
        return id;
    }

    public Participation getPm() {
        return pm;
    }

    public Iterator<Participation> getMembers() {
        return members.iterator();
    }

    public Set<Participation> getMembersSet() {return members; }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Set<Announcement> getAnnouncements() { return this.announcements; }

    public Set<Tag> getTags() {
        return tags;
    }

    public String getLogo() {
        return logo;
    }

    public void setPm(Participation pm) {
        this.pm = pm;
    }

    public void setMembers(Set<Participation> members) {
        this.members = members;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAnnouncements(Set<Announcement> announcements) { this.announcements = announcements; }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }

    public void removeTag(Tag tag) {
        tags.remove(tag);
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean hasUser(RegisteredUser user) {
        return this.pm.getUser().getID() == user.getID() || this.members
                .stream()
                .filter(p -> p.getUser().getID() == user.getID()).findFirst().isPresent();
    }

    public void addMember(Participation u) {
        this.members.add(u);
    }
}