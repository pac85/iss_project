package org.unipa.group2.ISS_Project.classes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

public interface User {
    Integer getID();
    String getName();
    String getSurname();
    String getPassword();
    LocalDate getBirthDate();
    Boolean getDisabled();
    String getAddress();
    String getPostcode();
    String getCV();
    String getPhoto();
    Iterator<Seniority> getSkills();
    String getEmail();
    void setID(Integer id);
    void setName(String name);
    void setSurname(String surname);
    void setPassword(String password);
    void setBirthDate(LocalDate birthDate);
    void setDisabled(Boolean Disabled);
    void setAddress(String address);
    void setPostcode(String postcode);
    void setCV(String cv);
    void setPhoto(String photo);
    void setSkills(Set<Seniority> skills);
    void setEmail(String email);
    public void addSeniority(Seniority seniority);
    public Optional<Seniority> removeSkillSeniority(Skill rs);
    public void removeSeniority(Seniority seniority);
}
