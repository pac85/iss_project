package org.unipa.group2.ISS_Project.classes;

import javax.persistence.*;

@Entity
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"announcement_id", "user_id"})}
)
public class Reply implements Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "announcement_id", nullable = false)
    private Announcement announcement;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private RegisteredUser user;

    public static enum State {
        PENDING, ACCEPTED, REJECTED, CONFIRMED;

        public Boolean isPendingOrAccepted() {
            return this == PENDING || this == ACCEPTED;
        }

        public Integer getOrdI() {
            switch (this) {
                case ACCEPTED: return 1;
                case PENDING: return 2;
                case REJECTED: return 3;
                case CONFIRMED: return 4;
                default: return 10000;
            }
        }
    }

    private State state;

    private Integer hours;

    public Reply() {}

    public Reply(Announcement announcement, RegisteredUser user) {
        this.announcement = announcement;
        this.user = user;
        this.state = State.PENDING;
        this.hours = null;
    }

    public Integer getId() {
        return id;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public RegisteredUser getUser() {
        return user;
    }

    public State getState() {
        return state;
    }

    public Integer getHours() {
        return hours;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Reply clone() {
        try {
            return (Reply) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
