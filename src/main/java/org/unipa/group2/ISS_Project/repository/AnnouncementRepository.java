package org.unipa.group2.ISS_Project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.unipa.group2.ISS_Project.classes.Announcement;

import java.util.Set;

public interface AnnouncementRepository extends JpaRepository<Announcement, Integer> {
    Set<Announcement> findByActive(Boolean active);
}
