package org.unipa.group2.ISS_Project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.unipa.group2.ISS_Project.classes.RegisteredUser;

public interface UserRepo extends JpaRepository<RegisteredUser, Long> {
    RegisteredUser findByNameAndIsDisabled(String name, boolean isDisabled);
    RegisteredUser findByEmailAndIsDisabled(String email, boolean isDisabled);
    RegisteredUser findByEmail(String email);
}
