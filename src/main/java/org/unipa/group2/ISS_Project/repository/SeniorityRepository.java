package org.unipa.group2.ISS_Project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.unipa.group2.ISS_Project.classes.RegisteredUser;
import org.unipa.group2.ISS_Project.classes.Seniority;
import org.unipa.group2.ISS_Project.classes.Skill;

public interface SeniorityRepository extends JpaRepository<Seniority, Long> {
    Seniority findById(Integer id);
    Seniority findByIdAndUser(Integer id, RegisteredUser user);
}
