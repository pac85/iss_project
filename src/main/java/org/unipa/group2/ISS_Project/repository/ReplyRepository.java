package org.unipa.group2.ISS_Project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.unipa.group2.ISS_Project.classes.RegisteredUser;
import org.unipa.group2.ISS_Project.classes.Reply;

import java.util.Set;

public interface ReplyRepository extends JpaRepository<Reply, Integer> {
    Set<Reply> findAllByUser(RegisteredUser user);
}