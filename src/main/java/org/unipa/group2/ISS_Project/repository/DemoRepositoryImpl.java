package org.unipa.group2.ISS_Project.repository;

import org.springframework.stereotype.Component;
import org.unipa.group2.ISS_Project.models.DemoModel;

@Component
public class DemoRepositoryImpl implements DemoRepositoryInterface {
    public DemoModel getDemo() {
        return new DemoModel("Ciao", "Mondo", "Da Java");
    }
}
