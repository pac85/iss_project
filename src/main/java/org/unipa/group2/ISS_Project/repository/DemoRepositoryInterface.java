package org.unipa.group2.ISS_Project.repository;

import org.unipa.group2.ISS_Project.models.DemoModel;

public interface DemoRepositoryInterface {
    public DemoModel getDemo();
}
