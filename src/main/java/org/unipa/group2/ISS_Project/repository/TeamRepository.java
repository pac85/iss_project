package org.unipa.group2.ISS_Project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.unipa.group2.ISS_Project.classes.RegisteredUser;
import org.unipa.group2.ISS_Project.classes.Team;

import java.util.Set;

public interface TeamRepository extends JpaRepository<Team, Integer> {
    Set<Team> findByActive(Boolean active);
}
