package org.unipa.group2.ISS_Project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.unipa.group2.ISS_Project.classes.RegisteredUser;
import org.unipa.group2.ISS_Project.classes.Skill;

public interface SkillsRepository extends JpaRepository<Skill, Long> {
    Skill findById(Integer id);
}
