package org.unipa.group2.ISS_Project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.unipa.group2.ISS_Project.classes.Tag;
import org.unipa.group2.ISS_Project.classes.Team;

public interface TagRepository  extends JpaRepository<Tag, Integer> {

}
