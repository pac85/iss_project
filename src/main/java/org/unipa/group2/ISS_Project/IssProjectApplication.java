package org.unipa.group2.ISS_Project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IssProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(IssProjectApplication.class, args);
	}

}
