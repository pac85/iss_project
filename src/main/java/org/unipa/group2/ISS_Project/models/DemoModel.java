package org.unipa.group2.ISS_Project.models;

import java.io.Serializable;

public class DemoModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private String attributo1, attributo2, attributo3;

    public DemoModel(String a1, String a2, String a3) {
        this.attributo1 = a1;
        this.attributo2 = a2;
        this.attributo3 = a3;
    }

    public String getAttributo1() {
        return attributo1;
    }

    public void setAttributo1(String attributo1) {
        this.attributo1 = attributo1;
    }

    public String getAttributo2() {
        return attributo2;
    }

    public void setAttributo2(String attributo2) {
        this.attributo2 = attributo2;
    }

    public String getAttributo3() {
        return attributo3;
    }

    public void setAttributo3(String attributo3) {
        this.attributo3 = attributo3;
    }
}
