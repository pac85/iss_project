package org.unipa.group2.ISS_Project.controller;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.unipa.group2.ISS_Project.classes.*;
import org.unipa.group2.ISS_Project.repository.*;
import org.unipa.group2.ISS_Project.utils.SimpleClasses;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class ApiController {

    @Autowired
    private HttpServletRequest request;

    //repositories
    @Autowired
    private SkillsRepository skillsRepository;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private SeniorityRepository seniorityRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private AnnouncementRepository announcementRepository;
    @Autowired
    private ReplyRepository replyRepository;

    @GetMapping("/api/unident/getSkills")
    List<Skill> getSkills() {
        return skillsRepository.findAll();
    }

    @GetMapping("/api/unident/isEmailUsed/{email}")
    Boolean isEmailUsed(@PathVariable String email) {
        return userRepo.findByEmail(email) != null;
    }

    @GetMapping("/api/unident/getTags")
    List<Tag> getTags() {
        return tagRepository.findAll();
    }

    @GetMapping("/api/addSkill/{skill_id}/{years}")
    Boolean addSkill(@PathVariable Integer skill_id, @PathVariable Integer years, Authentication authentication){
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        if(user == null) {
            return false;
        }

        Skill skill = skillsRepository.findById(skill_id);
        if(skill == null) {
            return false;
        }

        Seniority seniority = new Seniority(user, skill, years);

        //allows to update the number of years by deleting the already present skill
        user.removeSkillSeniority(seniority.getSkill()).ifPresent(s -> {
            //remove previous seniority with that skill
            userRepo.save(user);
            seniorityRepository.delete(s);
        });

        user.addSeniority(seniority);
        userRepo.save(user);

        return true;
    }

    @GetMapping("/api/removeSkill/{seniority_id}")
    Boolean removeSkill(@PathVariable Integer seniority_id, Authentication authentication) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        if(user == null) {
            return false;
        }

        Seniority seniority = seniorityRepository.findByIdAndUser(seniority_id, user);
        if(seniority == null) {
            return false;
        }

        user.removeSeniority(seniority);

        seniorityRepository.delete(seniority);

        return true;
    }

    @GetMapping("/api/addTag/{team_id}/{tag_id}")
    Boolean addTag(Authentication authentication, @PathVariable Integer team_id, @PathVariable Integer tag_id) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        if(user == null) {
            return false;
        }

        Team team = teamRepository.findById(team_id).get();
        Tag tag = tagRepository.findById(tag_id).get();
        if(team == null || tag == null) {
            return false;
        }
        //make sure the call is made by the pm
        if(team.getPm().getUser().getID() != user.getID()) {
            return false;
        }

        team.addTag(tag);

        teamRepository.save(team);

        return true;
    }

    @GetMapping("/api/removeTag/{team_id}/{tag_id}")
    public Boolean removeTag(Authentication authentication, @PathVariable Integer team_id, @PathVariable Integer tag_id) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        if(user == null) {
            return false;
        }

        Team team = teamRepository.findById(team_id).get();
        Tag tag = tagRepository.findById(tag_id).get();
        if(team == null || tag == null) {
            return false;
        }
        //make sure the call is made by the pm
        if(team.getPm().getUser().getID() != user.getID()) {
            return false;
        }

        //there must be at least one tag
        if(team.getTags().size() <= 1) {
            return false;
        }

        team.removeTag(tag);

        teamRepository.save(team);

        return true;
    }

    @GetMapping("/api/getTeamTags/{team_id}")
    public List<Tag> getTeamtags(@PathVariable Integer team_id) {
        Team team = teamRepository.findById(team_id).get();

        List<Tag> res = new ArrayList<>();
        team.getTags().forEach(t -> res.add(t));

        return res;
    }

    @GetMapping(value = "/api/getUserPropic", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getUserPropic(Authentication authentication) {

        String default_propic_path = "src/main/resources/static/defaultPropic.png";
        Path dpath = Paths.get(default_propic_path);

        //loads default propic, should mever fail
        byte[] defaultPropicData = null;
        try {
            defaultPropicData = Files.readAllBytes(dpath);
        } catch(IOException e) {
            System.out.println("\n\n\ncannot open default propic!!! \n\n\n");
        }

        //finds the path of the uploads dir
        String uploadsDir =  request.getServletContext().getRealPath("/");

        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        //if the user doesn't have a propic or is not found return the default one
        if(user == null) {
            return defaultPropicData;
        }

        String propic_path = user.getPhoto();
        if(propic_path == null) {
            return defaultPropicData;
        }

        //try to load the user propic
        Path path = Paths.get(uploadsDir + propic_path);
        try {
            return Files.readAllBytes(path);
        }
        catch (IOException e) {
            //return the defualt one if that fails
            return defaultPropicData;
        }
    }

    @GetMapping("/api/removeUserPropic")
    public Boolean removeUserPropic(Authentication authentication) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        if(user == null) {
            return false;
        }

        user.setPhoto(null);
        userRepo.save(user);
        return true;
    }

    @GetMapping("/api/getUserSkills")
    List<SimpleClasses.SimpleSeniority> getUserSkills(Authentication authentication) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        if(user == null) {
            return null;
        }

        List<SimpleClasses.SimpleSeniority> list = new ArrayList<>();
        //create a list of SimpleSeniority from th iterator
        user.getSkills().forEachRemaining(el -> list.add(new SimpleClasses.SimpleSeniority(el.getId(), new SimpleClasses.SimpleSkill(el.getSkill()), el.getYears())));
        return list;
    }

    //given a user it calculates the amount of hours the user has available
    public static Integer computeRemainingHours(TeamRepository teamRepository, RegisteredUser user) {
        Set<Team> teams = teamRepository.findByActive(true);

        Integer hours = 50 - teams
                .stream()
                .map(t -> {
                    //takes into account active teams in which the user is a pm
                    if(t.getPm().getUser().getID() == user.getID()) return t.getPm().getHours();
                    List<Participation> users = new ArrayList<>();
                    t.getMembers().forEachRemaining(m -> users.add(m));
                    //takes into account all active teams in which the user is a member
                    return users
                            .stream()
                            .filter(u -> u.getUser().getID() == user.getID())
                            .findFirst()
                            .map(u -> u.getHours())
                            .orElse(0);
                })
                .reduce(0, (a, b) -> a+b);

        return hours;
    }

    @GetMapping("/api/getRemainingHours")
    Integer getRemainingHours(Authentication authentication) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        if(user == null) {
            return 0;
        }

        return computeRemainingHours(teamRepository, user);
    }

    @GetMapping("/api/getTeams")
    List<Team> getTeams(Authentication authentication) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        if(user == null) return null;

        List<Team> teams = teamRepository.findAll();

        String uploadsDir =  request.getServletContext().getRealPath("/");
        return teams
                .stream()
                .filter(t -> t.hasUser(user))
                .map(u -> {
                    if(u.getLogo() == null || u.getLogo().isEmpty() || !(new File(uploadsDir + u.getLogo())).exists()) {
                        u.setLogo("/defaultTeamPic.png");
                    }

                    //returning a user object would expose sensitive information
                    u.setMembers(new HashSet<>());
                    u.setPm(null);
                    //an announcement has team which has a user so it can't be returned either
                    u.setAnnouncements(new HashSet<>());
                    return u;
                })
                .collect(Collectors.toList());
    }

    public static <T> Stream<T> streamFromIt(Iterator<T> t) {
        List a = new ArrayList();
        t.forEachRemaining(e -> a.add(e));

        return a.stream();
    }

    //verifies that a user has the required skills
    public static Boolean hasSkills(Announcement announcement, RegisteredUser user) {
        return announcement
                .getSkills()
                .stream()
                .allMatch(s -> streamFromIt(user.getSkills())
                        .anyMatch(us -> us.getSkill().getID().equals(s.getID()))
                );
    }

    //thid function tries to add a reply to an announcement.
    //it returns:
    // 0 in case of success,
    // 1 in case the user is the pm of the team of that announcement
    // 2 in case the user doesn't have the required skills
    @GetMapping("/api/replyToAnnouncement/{ann_id}")
    Integer replyToAnnouncement(Authentication authentication, @PathVariable Integer ann_id) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        Announcement announcement = announcementRepository.findById(ann_id).get();

        if(announcement.getTeam().getPm().getUser().getID().equals(user.getID())) {
            return 1;
        }

        if(!hasSkills(announcement, user)) {
            return 2;
        }

        announcement.addRepy(new Reply(announcement, user));

        announcementRepository.save(announcement);

        return 0;
    }

    //returns the html code for the navbar
    @GetMapping("/api/getNavbar")
    ModelAndView getNavbar(Authentication authentication) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);

        Long replies = replyRepository
                .findAllByUser(user)
                .stream()
                .filter(r -> r.getState() == Reply.State.ACCEPTED)
                .count();

        ModelAndView m = new ModelAndView("navbar");
        m.addObject("email", user.getEmail());
        m.addObject("replies", replies);

        return m;
    }

    //verify that the Reply number of hours meets the requirements
    public static Boolean checkReplyHours(Reply reply) {
        return reply.getHours() > 0;
    }

    @GetMapping("/api/acceptReply/{reply_id}/{hours}")
    Boolean acceptReply(Authentication authentication, @PathVariable Integer reply_id, @PathVariable Integer hours) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        Reply reply = replyRepository.findById(reply_id).orElse(null);
        if(reply == null || user == null) {
            return false;
        }

        if(!reply.getAnnouncement().getTeam().getPm().getUser().getID().equals(user.getID())) {
            return false;
        }

        reply.setState(Reply.State.ACCEPTED);
        reply.setHours(hours);

        if(!checkReplyHours(reply)) {
            return false;
        }

        replyRepository.save(reply);

        return true;
    }

    @GetMapping("/api/acceptHours/{reply_id}")
    Boolean acceptHours(Authentication authentication, @PathVariable Integer reply_id) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        Reply reply = replyRepository.findById(reply_id).orElse(null);
        if(reply == null || user == null) {
            return false;
        }

        if(!reply.getUser().getID().equals(user.getID())) {
            return false;
        }

        reply.setState(Reply.State.CONFIRMED);
        replyRepository.save(reply);

        if(reply.getHours() <= getRemainingHours(authentication)) {
            //add the user to the team
            Team team = reply.getAnnouncement().getTeam();
            Participation participation = new Participation(user, reply.getHours());
            participation.setTeam(team);
            team.addMember(participation);
            teamRepository.save(team);
        }
        else {
            return false;
        }

        return true;
    }
}
