package org.unipa.group2.ISS_Project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;
import org.unipa.group2.ISS_Project.classes.Participation;
import org.unipa.group2.ISS_Project.classes.RegisteredUser;
import org.unipa.group2.ISS_Project.classes.Tag;
import org.unipa.group2.ISS_Project.classes.Team;
import org.unipa.group2.ISS_Project.repository.TagRepository;
import org.unipa.group2.ISS_Project.repository.TeamRepository;
import org.unipa.group2.ISS_Project.repository.UserRepo;
import org.unipa.group2.ISS_Project.utils.ElClasses;
import org.unipa.group2.ISS_Project.utils.ISSUtils;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class TeamController {
    @Autowired
    private HttpServletRequest request;

    //repositories
    @Autowired
    TeamRepository teamRepository;
    @Autowired
    UserRepo userRepo;
    @Autowired
    TagRepository tagRepository;

    @GetMapping("/teamCreation")
    String teamCreationForm(Authentication authentication, Model model) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);

        model.addAttribute("remaining", ApiController.computeRemainingHours(teamRepository, user));

        return "teamCreation";
    }

    //checks various fields
    public static Boolean checkTeamName(String n) {
        return n.length() > 0 && n.length() <= 30;
    }

    public static Boolean checkTeamDescription(String d) {
        return d.length() > 0 && d.length() <= 1024;
    }

    public static Boolean checkTeamTag(Tag tag) {
        return tag != null;
    }

    public static Boolean checkTeamLogo(String fileName, Long size) {

        return (fileName.matches(".*[.](png|jpg|webp)") && size <= 8 * 1024 * 1024) || size == 0;

    }

    public static Boolean checkHoursPerWeek(Integer h, Integer available) {
        return h <= available;
    }

    @RequestMapping(path = "/teamCreation", method = POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public RedirectView teamCreation(
            Authentication authentication,
            @RequestParam(name="name", required=true) String name,
            @RequestParam(name="description", required=true) String description,
            @RequestParam(name="tag", required=true) Integer tag,
            @RequestParam(name="hours", required=true) Integer hours,
            @RequestPart(required = false) MultipartFile logo
    ) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        Tag tagObj = tagRepository.findById(tag).get();
        if(user == null ||
                !checkTeamName(name) ||
                !checkTeamDescription(description) ||
                !checkTeamTag(tagObj) ||
                !checkTeamLogo(logo.getOriginalFilename(), logo.getSize())
        ) {
            return new RedirectView("/error");
        }

        if (checkHoursPerWeek(ApiController.computeRemainingHours(teamRepository, user), hours)) {
            return new RedirectView("/teamCreation");
        }

        Participation pm = new Participation(user, hours);
        String logoFile = "";
        if(logo != null && !logo.isEmpty()) {
            logoFile = ISSUtils.saveFile(request, name, logo);
        }
        Team team = new Team(pm, new HashSet<>(), name, description, logoFile, new HashSet<>(), tagObj, new HashSet<>());
        pm.setTeam(team);

        teamRepository.save(team);

        return new RedirectView("/tagEditor.html?team_id="+team.getId());
    }

    @GetMapping("/showTeam")
    public String showTeam(
        @RequestParam(name="team_id", required=true) Integer team_id,
        Model model,
        Authentication authentication
    ) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        Team team = teamRepository.findById(team_id).get();
        String uploadsDir =  request.getServletContext().getRealPath("/");
        if(team.getLogo() == null || team.getLogo().isEmpty() || !(new File(uploadsDir + team.getLogo())).exists()) {
            team.setLogo("/defaultTeamPic.png");
        }
        model.addAttribute("name", team.getName());
        model.addAttribute("description", team.getDescription());
        model.addAttribute("logo", team.getLogo());
        model.addAttribute(
                "tags",
                team
                        .getTags()
                        .stream()
                        .map(ElClasses.TagEl::new)
                        .iterator());
        team.getMembersSet().add(team.getPm());
        model.addAttribute(
                "members",
                team
                        .getMembersSet()
                        .stream()
                        .map(Participation::getUser)
                        .map(u->{
                            //use the default propic if the file doesn't exist
                            if(!(new File(uploadsDir + u.getPhoto()).exists())) u.setPhoto("/defaultPropic.png");
                            return u;
                        })
                        .map(ElClasses.UserEl::new)
                        //used to pass role information to thymeleaf
                        .map(u -> u.withRole(u.id == team.getPm().getUser().getID()? "PM":"member"))
                        .iterator()
        );

        model.addAttribute(
                "announcements",
                team
                        .getAnnouncements()
                        .stream()
                        .map(a -> {
                            if(a.getDescription().length() < 80) {
                                return a;
                            }
                            a.setDescription(a.getDescription().substring(0, 80-3) + "...");
                            return a;
                        })
                        .iterator()
        );

        model.addAttribute("amIPm", user.getID() == team.getPm().getUser().getID());

        return "showTeam";
    }

    @GetMapping("/testPublishAnnouncement")
    public String testPublishAnnouncement() {
        return "publishAnnouncement";
    }
}
