package org.unipa.group2.ISS_Project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import org.unipa.group2.ISS_Project.classes.*;
import org.unipa.group2.ISS_Project.repository.AnnouncementRepository;
import org.unipa.group2.ISS_Project.repository.SkillsRepository;
import org.unipa.group2.ISS_Project.repository.TeamRepository;
import org.unipa.group2.ISS_Project.repository.UserRepo;
import org.unipa.group2.ISS_Project.utils.ElClasses;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
public class AnnouncementController {
    @Autowired
    private HttpServletRequest request;

    //repositories
    @Autowired
    TeamRepository teamRepository;

    @Autowired
    UserRepo userRepo;

    @Autowired
    AnnouncementRepository announcementRepository;

    @Autowired
    SkillsRepository skillsRepository;

    @GetMapping("/publishAnnouncement")
    String testPublishAnnouncement(Authentication authentication, Model model) {
        return "publishAnnouncement";
    }

    public static Boolean canPublishAnnouncement(Team team, RegisteredUser user) {
        return user.getID() == team.getPm().getUser().getID();
    }

    @RequestMapping(path = "/publishAnnouncement")
    public RedirectView register(
            @RequestParam(name="team_id", required=true) Integer team_id,
            @RequestParam(name="title", required=true) String title,
            @RequestParam(name="description", required=true) String description,
            @RequestParam(name="skills", required = true) String skills,
            Authentication authentication
    ) {


        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);

        Team team = teamRepository.findById(team_id).get();

        //retrieve skills sent from the front end encoded in the form of a comma separated list
        Set<Skill> sks = Arrays.asList(skills.split(","))
                                .stream()
                                .map(s -> skillsRepository.findById(Integer.parseInt(s)))
                                .collect(Collectors.toSet());


        if(sks.isEmpty() || !canPublishAnnouncement(team, user)) return new RedirectView("/error");

        Announcement announcement = new Announcement(team,title,description,sks);

        announcementRepository.save(announcement);

        return new RedirectView("/showTeam?team_id=" + team_id);
    }

    //returns all announcements that don't belong to the user
    public static Stream<Announcement> filterOthersAnnouncements(Stream<Announcement> announcements, RegisteredUser user) {
        return announcements
                .filter(a -> !a.getTeam().hasUser(user))
                .map(a -> {
                    if(a.getDescription().length() < 80) {
                        return a;
                    }
                    a.setDescription(a.getDescription().substring(0, 80-3) + "...");
                    return a;
                });
    }

    @GetMapping("/viewAnnouncements")
    String viewAnnouncements(Model model, Authentication authentication) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);

        model.addAttribute(
                "announcements",
                filterOthersAnnouncements(announcementRepository
                        .findByActive(true)
                        .stream(), user)
                        .iterator()
        );

        return "viewAnnouncements";
    }

    @GetMapping("/viewAnnouncement")
    String viewAnnouncement(@RequestParam(name="ann_id", required=true) Integer ann_id, Model model, Authentication authentication) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);

        model.addAttribute(
                "announcement",
                announcementRepository
                        .findById(ann_id)
                        .map(a -> {
                            RegisteredUser euser =  a.getTeam().getPm().getUser();
                            //use default propic if one isn't set
                            if(euser.getPhoto() == null || euser.getPhoto().isEmpty()) {
                                euser.setPhoto("/defaultPropic.png");
                            }

                            return a;
                        })
                        .get()
        );

        Boolean isPm = user.getID().equals(announcementRepository.findById(ann_id).get().getTeam().getPm().getUser().getID());

        model.addAttribute(
                "amIPm",
                isPm
        );

        //true if the user has replied to the announcement
        model.addAttribute(
                "hasReplied",
                announcementRepository
                        .findById(ann_id)
                        .get()
                        .getReplies()
                        .stream()
                        .anyMatch(a -> a
                                .getUser()
                                .getID()
                                .equals(user.getID())
                        )
        );

        model.addAttribute(
                "replies",
                announcementRepository
                        .findById(ann_id)
                        .get()
                        .getReplies()
                        .stream()
                        .filter(r -> r.getState() == Reply.State.PENDING || r.getState() == Reply.State.ACCEPTED)
                        .map(ElClasses.ReplyEl::new)
                        .iterator()
                );

        return "viewAnnouncement";
    }
}
