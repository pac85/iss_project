package org.unipa.group2.ISS_Project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.unipa.group2.ISS_Project.classes.RegisteredUser;
import org.unipa.group2.ISS_Project.classes.Reply;
import org.unipa.group2.ISS_Project.classes.Skill;
import org.unipa.group2.ISS_Project.classes.Tag;
import org.unipa.group2.ISS_Project.repository.ReplyRepository;
import org.unipa.group2.ISS_Project.repository.SkillsRepository;
import org.unipa.group2.ISS_Project.repository.TagRepository;
import org.unipa.group2.ISS_Project.repository.UserRepo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class UserController {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private SkillsRepository skillsRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authManager;

    @Autowired
    private ReplyRepository replyRepository;

    //runs when Spring has finished initializing, in this case it is used to load the super admin as well as tag and
    //skills to the database
    @EventListener
    public void appReady(ApplicationReadyEvent event) {
        if (userRepo.count() == 0) {
            System.out.println("Saving super admin");

            RegisteredUser user = new RegisteredUser.RegisteredUserBuilder("SuperAdmin", "-", "super.admin@iss.it")
                    .password(passwordEncoder.encode("root"))
                    .build();

            user.setDisabled(false);
            userRepo.save(user);
        }

        if(skillsRepository.count() == 0) {
            System.out.println("Saving skills");

            Skill skill = new Skill("Programmer");
            skillsRepository.save(skill);
            skill = new Skill("Designer");
            skillsRepository.save(skill);
            skill = new Skill("Software Analyst");
            skillsRepository.save(skill);
            skill = new Skill("Software Architect");
            skillsRepository.save(skill);
            skill = new Skill("Data scientist");
            skillsRepository.save(skill);
            skill = new Skill("Developer");
            skillsRepository.save(skill);
            skill = new Skill("Accounter");
            skillsRepository.save(skill);
        }

        if(tagRepository.count() == 0) {
            System.out.println("Saving tags");

            Tag tag = new Tag("IT");
            tagRepository.save(tag);
            tag = new Tag("Web");
            tagRepository.save(tag);
            tag = new Tag("Mobile");
            tagRepository.save(tag);
            tag = new Tag("Public administration");
            tagRepository.save(tag);
            tag = new Tag("Cybersecurity");
            tagRepository.save(tag);
            tag = new Tag("Bioinformatics");
            tagRepository.save(tag);
            tag = new Tag("Banking");
            tagRepository.save(tag);
            tag = new Tag("Market analysis");
            tagRepository.save(tag);
        }
    }

    //redirects to teamInterface if if / is visited
    @GetMapping("/")
    public RedirectView home() {


        return new RedirectView("teamInterface.html");
    }

    @GetMapping("/login")
    public ModelAndView testlogin(Authentication authentication) {
        if(authentication != null) {
            return new ModelAndView("showLoginError");
        }
        return new ModelAndView("login");
    }

    //check for various fields
    public static Boolean checkName(String name) {
        return name.toLowerCase().matches("([a-z]|’|`|\'| )+") && name.length() <= 30;
    }

    public static Boolean checkEmail(String email) {
        return email.toUpperCase(Locale.ROOT).matches("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$");
    }

    public static LocalDate checkBirthDate(String birthDate, DateTimeFormatter df) {
        try {
            //calculates age and makes sure it is within 16 and 100
            LocalDate date = LocalDate.parse(birthDate, df);
            Integer age = Period.between(date, LocalDate.now()).getYears();
            System.out.println(age);
            if(age >= 16 && age <= 100) {
                return date;
            }
        }
        catch (DateTimeParseException ignored) {
        }

        return null;
    }

    public static Boolean checkPassword(String password) {
        return password.length() > 8 &&
                password.matches(".*[a-z]+.*") &&
                password.matches(".*[A-Z]+.*") &&
                password.matches(".*\\d+.*") &&
                password.matches(".*(@|!|\\+|=|\\?|#|\\[|\\]|\\{|\\}|\\(|\\))+.*");
    }

    public void login(String user, String pass) {
        UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(user, pass);
        Authentication auth = authManager.authenticate(authReq);

        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);
        HttpSession session = request.getSession(true);
        session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, sc);
    }

    @RequestMapping(path = "/registration")
    public RedirectView register(
            @RequestParam(name="name", required=true) String name,
            @RequestParam(name="surname", required=true) String surname,
            @RequestParam(name="email", required=true) String email,
            @RequestParam(name="password", required=true) String password,
            @RequestParam(name="birthDate", required=false, defaultValue = "") String birthDate,
            @RequestParam(name="address", required=false, defaultValue = "") String address,
            @RequestParam(name="postcode", required=false, defaultValue = "") String postcode,
            //@RequestParam(name="skills", required=false, defaultValue = "") Set<String>,
            Model model,
            Authentication authentication
    ) {

        if(authentication != null) {
            return new RedirectView("/");
        }

        if(!checkName(name) || !checkName(surname) || !checkEmail(email) || !checkPassword(password)) {
            return new RedirectView("/registration");
        }
        if (userRepo.findByEmail(email) != null) {
            return new RedirectView("/registration");
        }

        RegisteredUser newUser = new RegisteredUser.RegisteredUserBuilder(name, surname, email)
                .password(passwordEncoder.encode(password))
                .build();

        if(!birthDate.equals("")) {
            LocalDate date = checkBirthDate(birthDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            if(date == null) {
                return new RedirectView("/registration");
            }
            newUser.setBirthDate(date);
        }

        if(!address.equals("")) {
            newUser.setAddress(address);
        }

        if(!postcode.equals("")) {
            newUser.setPostcode(postcode);
        }

        newUser.setDisabled(false);
        userRepo.save(newUser);

        login(email, password);
        return new RedirectView("/skillEditor.html");
    }

    @GetMapping("/registration")
    String regForm(Authentication authentication) {
        if(authentication != null) {
            return "regError";
        }
        return "registration";
    }

    @RequestMapping(path = "/userUpdate")
    public RedirectView userUpdate(
            @RequestParam(name="name", required=false) String name,
            @RequestParam(name="surname", required=false) String surname,
            @RequestParam(name="password", required=false) String password,
            @RequestParam(name="birthDate", required=false, defaultValue = "") String birthDate,
            @RequestParam(name="address", required=false, defaultValue = "") String address,
            @RequestParam(name="postcode", required=false, defaultValue = "") String postcode,
            //@RequestParam(name="skills", required=false, defaultValue = "") Set<String>,
            Model model,
            Authentication authentication
    ) {

        if(!checkName(name) || !checkName(surname) || (!checkPassword(password) && !password.isEmpty())) {
            return new RedirectView("/userUpdate?error");
        }

        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);

        if(name != null) {
            user.setName(name);
        }

        if(surname != null) {
            user.setSurname(surname);
        }

        if(password != null && !password.isEmpty()) {
            user.setPassword(passwordEncoder.encode(password));
        }

        if(!birthDate.equals("")) {
            LocalDate date = checkBirthDate(birthDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            if(date == null) {
                return new RedirectView("/userUpdate");
            }
            user.setBirthDate(date);
        }

        if(!address.equals("")) {
            user.setAddress(address);
        }

        if(!postcode.equals("")) {
            user.setPostcode(postcode);
        }

        userRepo.save(user);

        return new RedirectView("/skillEditor.html");
    }

    @GetMapping("/userUpdate")
    String userUpdate(Authentication authentication, Model model) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        model.addAttribute("user", user);
        return "userUpdate";
    }

    //used as in an iframe inside skillEditor
    @GetMapping("/CVUpload")
    String CVUploadGet() {
        return "CVUpload";
    }

    @RequestMapping(path = "/CVUpload", method = POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    String CVUpload(@RequestPart MultipartFile document, Authentication authentication) {

        String uploadsDir = "/uploads/";
        String realPathtoUploads =  request.getServletContext().getRealPath(uploadsDir);
        if(! new File(realPathtoUploads).exists())
        {
            new File(realPathtoUploads).mkdir();
        }

        System.out.println("Using path" + realPathtoUploads);

        String filename = "cv" + authentication.getName() + document.getOriginalFilename();
        try {
            document.transferTo(new File(realPathtoUploads + "/" + filename));
        }catch(IOException e){}

        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        if(user == null) {
            return "CVUpload";
        }
        user.setCV("/uploads/" + filename);
        userRepo.save(user);

        return "CVUpload";
    }

    //used as in an iframe inside skillEditor
    @GetMapping("/PropicUpload")
    String PropicUpload() {
        return "PropicUpload";
    }

    @RequestMapping(path = "/PropicUpload", method = POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    String PropicUpload(@RequestPart MultipartFile document, Authentication authentication) {

        String uploadsDir = "/uploads/";
        String realPathtoUploads =  request.getServletContext().getRealPath(uploadsDir);
        if(! new File(realPathtoUploads).exists())
        {
            new File(realPathtoUploads).mkdir();
        }

        System.out.println("Using path" + realPathtoUploads);

        String filename = "propic" + authentication.getName() + document.getOriginalFilename();
        try {
            document.transferTo(new File(realPathtoUploads + "/" + filename));
        }catch(IOException e){}

        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);
        if(user == null) {
            return "PropicUpload";
        }
        user.setPhoto("/uploads/" + filename);
        userRepo.save(user);

        return "PropicUpload";
    }

    //sorts replies by state
    public static List<Reply> sortReplies(Iterable<Reply> replies) {
        List<Reply> repliesList = new ArrayList();
        replies.forEach(r -> repliesList.add(r));

        repliesList.sort(Comparator.comparing(a -> a.getState().getOrdI()));

        return repliesList;
    }

    @GetMapping("/replyStatus")
    String replyStatus(Authentication authentication, Model model) {
        RegisteredUser user = userRepo.findByEmailAndIsDisabled(authentication.getName(), false);

        List<Reply> replies = sortReplies(replyRepository.findAllByUser(user));

        model.addAttribute("replies", replies.stream()/*.map(AnnouncementController.SimpleReply::new)*/.iterator());

        return "replyStatus";
    }

}
