skills_names = {};

function updateSkillsSelect() {
    var request = new XMLHttpRequest();
    request.open("GET", "/api/unident/getSkills");
    request.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) {
            const response_object = JSON.parse(this.responseText);
            var skills_html = "";
            response_object.forEach(function (v) {
                skills_names[v.id] = v.name;
                skills_html += "<option value=\""+v.id+"\">"+v.name+"</option>";
            });
            document.getElementById("new_skills").innerHTML = skills_html;
        }
    };
    request.send();
}

skills = [];

function removeSkill(skill_id) {
    skills = skills.filter(s => s != skill_id);
    updateList();
}

function updateList() {

    document.getElementById("form_skills").value = "";

    skills.forEach(v => document.getElementById("form_skills").value += v + ",");
    var form_skills = document.getElementById("form_skills").value;
    document.getElementById("form_skills").value = form_skills.substring(0, form_skills.length - 1);

    var skills_html = `
      <div class="row p-1">
                      <div class="col centered" style="font-weight: bold">Skill</div>
                      <div class="col-1">

                      </div>
                  </div>
                                      `;
    if(skills.length <= 0) {
        skills_html += `
                                    <div class="row p-1">
                                                    <div class="col centered">-</div>
                                                    <div class="col-1">

                                                    </div>
                                                </div>
                                                                    `;
    }
    skills.forEach(function (v) {
        var row_html = `
        <div class="row p-1">
                        <div class="col centered">${skills_names[v]}</div>
                        <div class="col-1">
                            <button onclick="removeSkill(${v})" type="button" class="btn btn-danger">Remove</button>
                        </div>
                    </div>
        `;
        skills_html += row_html;//"<div class=\"row p-1\">"+v.skill.name+"<a href=\"#\" onclick=\"removeSkill("+v.id+")\">remove</a></div>";
    });

    document.getElementById("skills").innerHTML = skills_html;
}

function addSkill() {
    const skill_id = document.getElementById("new_skills").value;

    if(skills.find(s => s === skill_id)) {
        return;
    }

    document.getElementById("skill_alert").style.display = 'none';
    skills.push(skill_id);

    updateList();
}

function submitF() {
    if(skills.length == 0) {
        document.getElementById("skill_alert").style.display = 'block';
        return;
    }
    document.getElementById('submit_button').click();
}