function pullNavbar(navId) {
    var request = new XMLHttpRequest();
        request.open("GET", "/api/getNavbar");
        request.onreadystatechange = function() {
            if(this.readyState === 4 && this.status === 200) {
                const response_object = this.responseText;

                document.getElementById(navId).innerHTML = response_object;
            }
        };
        request.send();
}

pullNavbar("navid");