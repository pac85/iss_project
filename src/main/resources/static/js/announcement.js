function replyToAnnouncement(ann_id) {
    var request = new XMLHttpRequest();
    request.open("GET", "/api/replyToAnnouncement/"+ann_id);
    request.onreadystatechange = function() {
        const response_object = JSON.parse(this.responseText);
        if(response_object == 0) {
            document.getElementById("reply_id").disabled = true;
            document.getElementById("reply_id").innerHTML = "You've already replied";
        } else if(response_object == 1) {
            //internal error
        }
        else if(response_object == 2) {
            //skills
            alert("you don't have the required skills");
        }
    };
    request.send();
}

function acceptReply(reply_id, hid) {
    hours = document.getElementById(hid).value;
    if(hours <= 0) {
        alert("Enter the number of hours");
        return;
    }
    var request = new XMLHttpRequest();
    request.open("GET", "/api/acceptReply/"+reply_id+"/"+hours);
    request.onreadystatechange = function() {
        location = "";
        const response_object = JSON.parse(this.responseText);
        if(response_object) {
            //document.getElementById("reply_id").disabled = true;
        } else {
            //internal error
        }
    };
    request.send();
}

function acceptHours(reply_id) {

    var request = new XMLHttpRequest();
    request.open("GET", "/api/acceptHours/"+reply_id);
    request.onreadystatechange = function() {
        location = "";
        const response_object = JSON.parse(this.responseText);
        if(response_object) {
            //document.getElementById("reply_id").disabled = true;
        } else {
            //internal error
        }
    };
    request.send();
}