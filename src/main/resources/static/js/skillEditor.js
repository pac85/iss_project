function addSkill() {
    const skill_id = document.getElementById("new_skills").value;
    const years = document.getElementById("years").value;

    if(years <= 0) {
        return;
    }

    var request = new XMLHttpRequest();
    request.open("GET", "/api/addSkill/"+skill_id+"/"+years);
    request.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) {
            const response_object = JSON.parse(this.responseText);
            updateList();
        }
    };
    request.send();
}

function removeSkill(id) {
    var request = new XMLHttpRequest();
    request.open("GET", "/api/removeSkill/"+id);
    request.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) {
            const response_object = JSON.parse(this.responseText);
            updateList();
        }
    };
    request.send();
}

function updateList() {
    var request = new XMLHttpRequest();
    request.open("GET", "/api/getUserSkills");
    request.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) {
            const response_object = JSON.parse(this.responseText);
            var skills_html = `
              <div class="row p-1">
                              <div class="col centered" style="font-weight: bold">Skill</div>
                              <div class="col-1 centered" style="font-weight: bold">Years</div>
                              <div class="col-1">

                              </div>
                          </div>
                                              `;
            if(response_object.length <= 0) {
                skills_html += `
                                            <div class="row p-1">
                                                            <div class="col centered">-</div>
                                                            <div class="col-1 centered">-</div>
                                                            <div class="col-1">

                                                            </div>
                                                        </div>
                                                                            `;
            }
            response_object.forEach(function (v) {
                var row_html = `
                <div class="row p-1">
                                <div class="col centered">${v.skill.name}</div>
                                <div class="col-1 centered">${v.years}</div>
                                <div class="col-1">
                                    <button onclick="removeSkill(${v.id})" type="button" class="btn btn-danger">Remove</button>
                                </div>
                            </div>
                `;
                skills_html += row_html;//"<div class=\"row p-1\">"+v.skill.name+"<a href=\"#\" onclick=\"removeSkill("+v.id+")\">remove</a></div>";
            });
            document.getElementById("skills").innerHTML = skills_html;
        }
    };
    request.send();
}

function updateSkillsSelect() {
    var request = new XMLHttpRequest();
    request.open("GET", "/api/unident/getSkills");
    request.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) {
            const response_object = JSON.parse(this.responseText);
            var skills_html = "";
            response_object.forEach(function (v) {
                skills_html += "<option value=\""+v.id+"\">"+v.name+"</option>";
            });
            document.getElementById("new_skills").innerHTML = skills_html;
        }
    };
    request.send();
}
updateList();
updateSkillsSelect();
