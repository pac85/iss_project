 function showTeams(teams) {
    //var teams = [{name: "team1", description: "A crappy team", logo: "https://blog.udemy.com/wp-content/uploads/2014/05/bigstock-test-icon-63758263.jpg"}, {name: "team2", description: "Another crappy team", logo: "https://blog.udemy.com/wp-content/uploads/2014/05/bigstock-test-icon-63758263.jpg"}];


    var max_len = 90;

    var out_html = "";
    var c=0;

    if(teams.length == 0) {
        out_html = `<div class="row mt-4">
                        <div class="col">
                                            <center>
                                                <div class="card" style="width: 18rem;">
                                                    <img style="overflow: hidden" height="286px" src="/No_team.png" class="card-img-top" alt="...">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Empty</h5>
                                                        <p class="card-text" style="height: 72px">You are not part of any team yet</p>
                                                        <a href="/viewAnnouncements" class="btn btn-primary"> Search announcements </a>
                                                    </div>
                                                </div>
                                            </center>
                                        </div>`;
    }
    teams.forEach(function (v) {
        if(c != 0 && c % 3 == 0) {
            out_html += `</div>`;
        }
        if(c % 3 == 0) {
            out_html += `<div class="row mt-4">`;
        }

        if(v.description.length > max_len) {
            v.description = v.description.substring(0, max_len-3) + "...";
        }

        inactive_class = "";
        if(!v.active) {
            inactive_class = "inactive-team";
        }

        tag_chips = "<div>";
        var col = {r: 30+Math.round(Math.random()*210), g: 30+Math.round(Math.random()*210), b: 30+Math.round(Math.random()*210)};
        col = `rgb(${col.r}, ${col.g}, ${col.b})`;
        v.tags.forEach(function(t) {
            tag_chips += `<span class="badge my-badge" style="background: ${col}">${t.tag}</span>`;
        });
        tag_chips += "</div>";
        out_html += `
        <div class="col">
                        <center>
                            <div class="card ${inactive_class}" style="width: 18rem;">
                                <img style="overflow: hidden" height="286px" src="${v.logo}" class="card-img-top" alt="...">
                                ${tag_chips}
                                <div class="card-body">
                                    <h5 class="card-title">${v.name}</h5>
                                    <p class="card-text" style="height: 72px">${v.description}</p>
                                    <a href="/showTeam?team_id=${v.id}" class="btn btn-primary">View</a>
                                </div>
                            </div>
                        </center>
                    </div>
        `;
        c += 1;
    });

    out_html += `</div>`;

    document.getElementById("teams").innerHTML = out_html;

 }

 //showTeams([{name: "team1", description: "A crappy team", logo: "https://blog.udemy.com/wp-content/uploads/2014/05/bigstock-test-icon-63758263.jpg"},{name: "team1", description: "A crappy team", logo: "https://blog.udemy.com/wp-content/uploads/2014/05/bigstock-test-icon-63758263.jpg"},{name: "team1", description: "A crappy team", logo: "https://blog.udemy.com/wp-content/uploads/2014/05/bigstock-test-icon-63758263.jpg"},{name: "team1", description: "A crappy team", logo: "https://blog.udemy.com/wp-content/uploads/2014/05/bigstock-test-icon-63758263.jpg"},{name: "team1", description: "A crappy team", logo: "https://blog.udemy.com/wp-content/uploads/2014/05/bigstock-test-icon-63758263.jpg"},{name: "team1", description: "A crappy team", logo: "https://blog.udemy.com/wp-content/uploads/2014/05/bigstock-test-icon-63758263.jpg"}, {name: "team2", description: "Another crappy team", logo: "https://blog.udemy.com/wp-content/uploads/2014/05/bigstock-test-icon-63758263.jpg"}]);

function fetchTeams() {
    var request = new XMLHttpRequest();
    request.open("GET", "/api/getTeams");
    request.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) {
            const response_object = JSON.parse(this.responseText);
            showTeams(response_object);
        }
    };
    request.send();
}

fetchTeams();