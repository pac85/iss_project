team_id = "";

function getTeamId() {
    address = window.location.search;
    parameterList = new URLSearchParams(address);
    parameterList.forEach((value, key) => {
        if(key === "team_id") {
            team_id = value;
        }
    })
}
getTeamId()

function addTag() {
    const tag_id = document.getElementById("new_tags").value;

    var request = new XMLHttpRequest();
    request.open("GET", "/api/addTag/"+team_id+"/"+tag_id);
    request.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) {
            const response_object = JSON.parse(this.responseText);
            updateList();
        }
    };
    request.send();
}

function removeTag(id) {
    var request = new XMLHttpRequest();
    request.open("GET", "/api/removeTag/"+team_id+"/"+id);
    request.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) {
            const response_object = JSON.parse(this.responseText);
            updateList();
        }
    };
    request.send();
}

function updateList() {
    var request = new XMLHttpRequest();
    request.open("GET", "/api/getTeamTags/"+team_id);
    request.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) {
            const response_object = JSON.parse(this.responseText);
            var skills_html = `
              <div class="row p-1">
                              <div class="col centered" style="font-weight: bold">Tags</div>
                              <div class="col-1">
                              </div>
                          </div>
                                              `;
            if(response_object.length <= 0) {
                skills_html += `
                                            <div class="row p-1">
                                                            <div class="col centered">-</div>
                                                            <div class="col-1">

                                                            </div>
                                                        </div>
                                                                            `;
            }
            response_object.forEach(function (v) {
                var row_html = `
                <div class="row p-1">
                                <div class="col centered">${v.tag}</div>
                                <div class="col-1">
                                    <button onclick="removeTag(${v.id})" type="button" class="btn btn-danger">Remove</button>
                                </div>
                            </div>
                `;
                skills_html += row_html;//"<div class=\"row p-1\">"+v.skill.name+"<a href=\"#\" onclick=\"removeTag("+v.id+")\">remove</a></div>";
            });
            document.getElementById("tags").innerHTML = skills_html;
        }
    };
    request.send();
}

function updateTagsSelect() {
    var request = new XMLHttpRequest();
    request.open("GET", "/api/unident/getTags");
    request.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) {
            const response_object = JSON.parse(this.responseText);
            var skills_html = "";
            response_object.forEach(function (v) {
                skills_html += "<option value=\""+v.id+"\">"+v.tag+"</option>";
            });
            document.getElementById("new_tags").innerHTML = skills_html;
        }
    };
    request.send();
}
//updateList();
updateTagsSelect();
