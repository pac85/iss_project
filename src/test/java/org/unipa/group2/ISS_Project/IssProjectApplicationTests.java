package org.unipa.group2.ISS_Project;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.unipa.group2.ISS_Project.classes.*;
import org.unipa.group2.ISS_Project.controller.AnnouncementController;
import org.unipa.group2.ISS_Project.controller.ApiController;
import org.unipa.group2.ISS_Project.controller.UserController;
import org.unipa.group2.ISS_Project.controller.TeamController;
import org.unipa.group2.ISS_Project.repository.UserRepo;
import org.unipa.group2.ISS_Project.service.UserDetailsServiceImpl;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
class IssProjectApplicationTests {

	private TestRestTemplate restTemplate = new TestRestTemplate();

	@Autowired
	UserRepo ur;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Test
	void contextLoads() {

	}

	@Test
	void testLoginMail() throws Exception {
		UserDetailsServiceImpl u = new UserDetailsServiceImpl(ur);


		//must not throw exception
		u.processLogin("super.admin@iss.it");
	}

	@Test()
	void testLoginMailFail() throws Exception {
		UserDetailsServiceImpl u = new UserDetailsServiceImpl(ur);

		try {
			u.processLogin("wrong.email@iss.it");
			throw new Exception(); //an exception should have been thrown
		}
		catch(UsernameNotFoundException e) {

		}
	}

	@Test
	void testRightPassword() throws Exception {
		UserDetailsServiceImpl u = new UserDetailsServiceImpl(ur);

		assert(passwordEncoder.matches("root", u.processLogin("super.admin@iss.it").getPassword()));
	}

	@Test
	void testWrongPassword() throws Exception {
		UserDetailsServiceImpl u = new UserDetailsServiceImpl(ur);

		assert (!passwordEncoder.matches("wrong", u.processLogin("super.admin@iss.it").getPassword()));
	}

	@Test
	void testNameCheck() {
		assert(UserController.checkName("Salvatore"));
		assert(UserController.checkName("Ma'awiya Malubulu"));
		assert(!UserController.checkName("Pentagonododecaedrico tetraedrico"));
		assert(!UserController.checkName("Roberto-Francesco"));
		assert(UserController.checkName("Roberto Francesco"));
	}

	@Test
	void testSurameCheck() {
		assert(UserController.checkName("Messina"));
		assert(UserController.checkName("D’Amico"));
		assert(UserController.checkName("Da Vinci"));
		assert(!UserController.checkName("Pentagonododecaedrico tetraedrico"));
		assert(!UserController.checkName("Ryce-James"));
		assert(UserController.checkName("Ryce James"));
	}

	@Test
	void testBirtDateCheck() {
		assert(UserController.checkBirthDate("30/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy")) != null);
		assert(UserController.checkBirthDate("15/01/1910", DateTimeFormatter.ofPattern("dd/MM/yyyy")) == null);
		assert(UserController.checkBirthDate("29/09/2010", DateTimeFormatter.ofPattern("dd/MM/yyyy")) == null);
	}

	@Test
	void testEmailCheck() {
		assert(UserController.checkEmail("antonino.maniscalco02@you.unipa.it"));
		assert(!UserController.checkEmail("salva+tore#you.unipa.it"));
		assert(!UserController.checkEmail("a.n.d.r.e.a;M@"));
	}

	@Test
	void testPasswordCheck() {
		assert(UserController.checkPassword("Password0_1!"));
		assert(!UserController.checkPassword("Password"));
		assert(!UserController.checkPassword("Password0"));
		assert(!UserController.checkPassword("12345678"));
	}

	@Test
	void testTeamName() {
		assert(TeamController.checkTeamName("Deer"));
		assert(TeamController.checkTeamName("Keyboard and mouse"));
		assert(!TeamController.checkTeamName(""));
		assert(!TeamController.checkTeamName("Pentagonododecaedrico tetraedrico"));
		assert(TeamController.checkTeamName("Str1ng!"));
	}

	@Test
	void testTeamDescription() {
		assert(!TeamController.checkTeamDescription("a".repeat(1025)));
		assert(TeamController.checkTeamDescription("a".repeat(512)));
		assert(!TeamController.checkTeamDescription(""));
	}

	@Test
	void testTeamTag() {
		assert(TeamController.checkTeamTag(new Tag("Web")));
		assert(!TeamController.checkTeamTag(null));
	}

	@Test
	void testTeamLogoName() {
		assert(TeamController.checkTeamLogo("Propic.png", 1L));
		assert(TeamController.checkTeamLogo("Propic.jpg", 1L));
		assert(TeamController.checkTeamLogo("Propic.webp", 1L));
		assert(!TeamController.checkTeamLogo("Propic.pdf", 1L));
	}

	@Test
	void testTeamLogoSize() {
		assert(!TeamController.checkTeamLogo("Propic.png", 12 * 1024 * 1024L));
		assert(TeamController.checkTeamLogo("Propic.png", 100 * 1024L));
		assert(TeamController.checkTeamLogo("Propic.png", 8 * 1024 * 1024L));
		assert(!TeamController.checkTeamLogo("Propic.png", 9 * 1024 * 1024L));
	}

	@Test
	void testRemainingHoursPerWeek() {
		assert(TeamController.checkHoursPerWeek(25, 30));
		assert(!TeamController.checkHoursPerWeek(40, 30));
	}

	//verifies that the replies are sorted by state
	public static Boolean checkReplyOrder(Stream<Reply> replies) {
		Integer prev = -100000;
		for(Reply rep: replies.collect(Collectors.toList())) {
			if(rep.getState().getOrdI() < prev) {
				return false;
			}

			prev = rep.getState().getOrdI();
		}

		return true;
	}

	@Test
	void testReplySort() {
		List<Reply> replies = new ArrayList<>();

		Reply reply = new Reply();

		reply.setState(Reply.State.REJECTED);
		replies.add(reply.clone());

		reply.setState(Reply.State.PENDING);
		replies.add(reply.clone());

		reply.setState(Reply.State.CONFIRMED);
		replies.add(reply.clone());

		reply.setState(Reply.State.ACCEPTED);
		replies.add(reply.clone());

		//replies are out of order, the check should fail
		assert(!checkReplyOrder(replies.stream()));
		replies = UserController.sortReplies(replies);
		//in this case they are in order so it should not fail
		assert(checkReplyOrder(replies.stream()));
	}

	@Test
	void testReplyRemainingHoursPerWeek() {
		assert(TeamController.checkHoursPerWeek(25, 30));
		assert(!TeamController.checkHoursPerWeek(40, 30));
	}

	@Test
	void testSkillCheck() {
		Announcement announcement = new Announcement();

		Skill a = new Skill("FRONT_END");
		a.setID(1);
		Skill b = new Skill("BACK_END");
		b.setID(2);
		announcement.setSkills(new HashSet<>());
		announcement.getSkills().add(a);
		announcement.getSkills().add(b);

		RegisteredUser user = new RegisteredUser();
		user.setSkills(new HashSet<>());
		user.addSeniority(new Seniority(user, a, 10));
		//user only has the "FRONT_END" skill, the announcement also requires "BACK_END" so it shouldn't pass the check
		assert(!ApiController.hasSkills(announcement, user));
		user.addSeniority(new Seniority(user, b, 10));
		//now the user has both skills so it should pass the check
		assert(ApiController.hasSkills(announcement, user));
	}

	@Test
	void testHoursCheck() {
		Reply reply = new Reply();
		reply.setHours(12);
		assert(ApiController.checkReplyHours(reply));
		reply.setHours(0);
		assert(!ApiController.checkReplyHours(reply));
		reply.setHours(-1);
		assert(!ApiController.checkReplyHours(reply));
	}

	@Test
	void testPmCheck() {
		RegisteredUser a = new RegisteredUser();
		a.setID(1);
		RegisteredUser b = new RegisteredUser();
		b.setID(2);

		Team team = new Team();
		team.setPm(new Participation(b, 10));

		//a is not the PM so it must not pass the check
		assert(!AnnouncementController.canPublishAnnouncement(team, a));
		//b is the PM so it should pass the check
		assert(AnnouncementController.canPublishAnnouncement(team, b));
	}
}
